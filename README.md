# Matrix Service
This project contains ansible playbook to deploy:
- Matrix Home Server: [Synapse](https://github.com/matrix-org/synapse)
- Matrix Web Client: [Riot-Web](https://github.com/vector-im/riot-web)
- Matrix-Telegram bridge: [Mautrix-Telegram](https://github.com/tulir/mautrix-telegram)
- Reverse proxy for Web: [Traefik](https://traefik.io/)

This is primarily used to deploy Matrix service for [FSCI](https://fsci.in/).
The project was originally meant to deploy only the bridge. But it grew in scope
to include the entire service. This source tree will be merged with
[fsci/infrastructure-ansible](https://gitlab.com/fsci/infrastructure-ansible)
project after completion and refactoring.

## Tasks
- VM related:
    - [x] Secure SSH
    - [x] Deploy Firewall
    - [x] Deploy docker-ce and docker-compose
    - [ ] Deploy fail2ban
    - [x] Deploy reverse proxy (traefik)
- Administration related:
    - [ ] Deploy Logging
    - [ ] Deploy Monitoring
    - [ ] Create Backup solution
- Service related:
    - [x] Deploy Home Server
    - [x] Deploy Web Client
    - [ ] Deploy Telegram Bridge
    - [ ] Open service ports, close everything else
    - [ ] Enable TLS
- Code refactoring:
    - [ ] Provision external password store
    - [ ] Parametrize hostname
    - [ ] Remove Admin Web UI
    - [ ] Modularize
    - [ ] Fix containers versions
    - [ ] Change to Buster
    - [ ] Integrate with FSCI repo

## License
Copyright (c) Gokul Das B

The contents of this source tree are under GNU General Public License,
Version 3 or above.
