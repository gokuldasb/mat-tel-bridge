---
# FSCI Matrix Project
# Ansible playbook to deploy Matrix service
#
# Docker CE install is based on:
#     https://docs.docker.com/install/linux/docker-ce/debian/

- name: Secure SSH
  hosts: bridge_host
  tasks:
  - name: Disable password login for SSH
    become: yes
    replace:
      path: /etc/ssh/sshd_config
      regexp: '^#?PasswordAuthentication yes'
      replace: PasswordAuthentication no
    notify: restart sshd
  - name: Disable root login for SSH
    become: yes
    replace:
      path: /etc/ssh/sshd_config
      regexp: '^#?PermitRootLogin.*'
      replace: PermitRootLogin no
    notify: restart sshd
  handlers:
    - name: restart sshd
      become: yes
      command: service ssh restart

- name: Install distro packages
  hosts: bridge_host
  tasks:
  - name: Update everything
    become: yes
    apt:
      update_cache: yes
      upgrade: yes
  - name: Install Docker-CE prerequisites and UFW
    become: yes
    apt:
      state: latest
      name:
        - apt-transport-https
        - ca-certificates
        - curl
        - gnupg2
        - software-properties-common
        - python-pip
        - ufw

- name: Deploy firewall
  hosts: bridge_host
  tasks:
  - name: Start firewall
    become: yes
    systemd:
      name: ufw.service
      state: stopped
      # TODO: Enable Later
      enabled: yes
  - name: Configure & enable firewall
    become: yes
    ufw:
      name: OpenSSH
      rule: allow
      state: enabled

- name: Deploy Docker and Compose
  hosts: bridge_host
  tasks:
  - name: Create docker group
    become: yes
    group:
      name: docker
      state: present
  - name: Add user to docker group
    become: yes
    user:
      name: "{{ ansible_ssh_user }}"
      groups: docker
      append: yes
  - name: Add Docker GPG key to Apt
    become: yes
    apt_key:
      state: present
      url: https://download.docker.com/linux/debian/gpg
      id: 8D81803C0EBFCD88
  - name: Add Docker stable Debian repository to Apt
    become: yes
    apt_repository:
      repo: deb [arch=amd64] https://download.docker.com/linux/debian stretch stable
      state: present
  - name: Install Docker-CE
    become: yes
    apt:
      update_cache: yes
      state: latest
      name:
        - docker-ce
        - docker-ce-cli
        - containerd.io
  - name: Ensure that docker daemon is enabled and running
    become: yes
    systemd:
      name: docker.service
      state: started
      enabled: yes
  - name: Install docker-compose
    become: yes
    pip:
      name: docker-compose
      state: latest

- name: Deploy service
  hosts: bridge_host
  tasks:
  - name: Copy over server files and directories
    # TODO: Remove secrets
    # TODO: Determine proper permissions 
    become: yes
    copy:
      src: srv
      dest: /
  - name: Setup Matrix services
    docker_compose:
      project_src: /srv/compose/matrix
      state: present
    tags:
      - matrix-deploy
  - name: Restart Matrix services
    docker_compose:
      project_src: /srv/compose/matrix
      state: present
      restarted: yes
    tags:
      - never
      - matrix-restart
  - name: Stop Matrix services
    docker_compose:
      project_src: /srv/compose/matrix
      state: present
      stopped: yes
    tags:
      - never
      - matrix-stop
