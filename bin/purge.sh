if [ ! $# -eq 1 ]; then
    exit 1
fi

ssh -t $1 "cd /srv/compose/matrix; docker-compose down"
ssh -t $1 "sudo rm -rf /srv"
